import React from 'react';
import Login from './pages/Login';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import SignUp from './pages/SignUp';
import AuthenticatedRoute from './AuthenticatedRoute';
import { ClientSesssionStore } from './store/ClientSessionStore';
import { observer, inject } from 'mobx-react';
import './styles/geral.css';

import MainRoutes from './MainRoutes';
// import RouteProtected from './route.protected';
// import {ProtectedRoute} from './ProtectedRoute';
// import { ProtectedRoute } from './protected.route';

interface RoutesProps {
    clientSession ?: ClientSesssionStore;
}

const Routes: React.FC<RoutesProps> = inject('clientSession')(observer((props) => {
    // const clientSessionStore = useContext(ClientSesssionStoreContext);
    return (
        <Router>
            <div>
                {/* <Main>
                    <Switch>
                        <Route path={[`/main/search/:titulo`, `/main/search/`]} component={Search} />
                        <Route path={[`/main/carrinho`]} component={Carrinho} />
                        <Route path={[`/main/livros/:id`]} component={LivroInfo} />
                        <Route path={[`/main/cliente`]} component={ClienteAccount} />
                    </Switch>
                </Main> */}
                <Switch>
                    <Route path={["/signup"]} component={SignUp} />
                    <Route exact path={["/"]} component={Login} />
                    <AuthenticatedRoute authenticated={!!props.clientSession!.cliente.id}
                        path={["/main"]}
                        component={MainRoutes}
                        redirectPath={"/"}
                        accessDenied="Você não está autenticado!"
                    />
                </Switch>
            </div>
        </Router>
    );
}));

export default Routes;