import React from 'react';
import LoginForm from '../../components/LoginForm';
import { Typography } from 'antd';
import './style.css';

const { Title } = Typography;

const Login: React.FC = () => {
    return (
        <div className="wrapper">
            <div className="form-wrapper">
                <Title>Log In</Title>
                <LoginForm />
            </div>
        </div>
    )
}

export default Login;