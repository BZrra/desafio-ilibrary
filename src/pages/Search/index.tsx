import React, { useEffect, useState } from 'react';
import CardsLivros from '../../components/CardsLivros';
// import Grid from '../components/Grid';
import LivroResource from '../../services/resources/LivroResource';
import objectFactory from '../../model/objectFactory';
import { RouteComponentProps } from 'react-router-dom';
import { Icon, List, message, Select } from 'antd';
import Genero from '../../model/Genero';
import GeneroResource from '../../services/resources/GeneroResource';

const { Option } = Select;

interface Props extends RouteComponentProps<any> {
    titulo: string;
}

const Search: React.FC<Props> = (props) => {
    const [livros, setLivros] = useState([
        new objectFactory().createLivro(0, '', '', 0, [''], '')
    ]);
    const [loading, setLoading] = useState(true);
    const [generos, setGeneros] = useState([new Genero(0, '')]);
    const [searchGenero, setSearchGenero] = useState('');

    useEffect(() => {
        GeneroResource().getGenero()
            .subscribe({
                next: res => { setGeneros(res); },
                error: err => { throw Error(err); }
            });
    }, []);

    useEffect(() => {
        const { titulo } = props.match.params;
        setLoading(true);
        // let response = await LivroResource().findPage(titulo, searchGenero);
        // setLivros(response.content);
        // response = await GeneroResource().getGenero();
        // setGeneros(response);
        try {
            LivroResource().findPage(titulo, searchGenero)
                .subscribe({
                    next: res => { setLivros(res.content); },
                    error: err => { throw Error(err); }
                });
        } catch (err) {
            message.error(err);
        }
        setLoading(false);
        
        // const response = await GeneroResource().getGenero();
        // setGeneros(response);

    }, [props.match.params, searchGenero]);

    if (!loading) {
        return (
            <div className="container">
                <Select defaultValue={""} style={{ width: 120, marginBottom: '15px' }} onChange={setSearchGenero}>
                    <Option value={""} key={0}>Todos</Option>
                    {generos.map((genero: Genero) => <Option value={genero.id.toString()} key={genero.id}>{genero.nome}</Option>)}
                </Select>
                <div className="main-search main">
                    <List
                        grid={{ gutter: 16, column: 4 }}
                        dataSource={livros}
                        locale={{ emptyText: 'Nenhum resultado encontrado!' }}
                        renderItem={item => (
                            <List.Item key={item.id}>
                                <CardsLivros livro={item} />
                            </List.Item>
                        )}
                    />

                </div>
            </div>
        );
    } else {
        return <Icon type="loading" />;
    }
}

export default Search;