import React from 'react';
import TableCarrinho from '../../components/TableCarrinho';
import CardSumario from '../../components/CardSumario';
import { Typography, Row, Col } from 'antd';
// import { useSelector } from 'react-redux';
// import { StoreState } from '../../store/types';
import {RouteComponentProps, withRouter, Link} from 'react-router-dom';
import { observer, inject } from 'mobx-react'; 
import { ShoppStore } from '../../store/ShoppStore';

const { Title, Paragraph } = Typography;

interface CarrinhoProps extends RouteComponentProps<any>{
    shopp: ShoppStore
}

const Carrinho: React.FC<CarrinhoProps> = inject('shopp')(observer((props) => {

    // const carrinho = useSelector((state: StoreState) => state.shopp.carrinho);
        // const shoppStore = useContext(ShoppStoreContext);

    if (props.shopp.getCarrinho.length) {
        return (
            <div className='main-carrinho main'>
                <Title>Carrinho</Title>
                <Row gutter={16}>
                    <Col span={16}>
                        <TableCarrinho />
                    </Col>
                    <Col span={6}>
                        <CardSumario />
                    </Col>
                </Row>
            </div>
        )
    }
    else {
        return (
            <div className='main-carrinho'>
                <Row>
                    <Col span={24}>
                        <Title level={2}>Carrinho Vazio!</Title>
                        <Paragraph>
                            Acesse a <Link to={'/'}>Página inicial</Link> para continuar as compras!
                        </Paragraph>
                    </Col>
                </Row>
            </div>
        )
    }

}));

export default withRouter(Carrinho);