import React from 'react';
import { Carousel } from 'antd';
import './style.css';
import banner1 from '../../images/banner/Exemplo-Banner.jpg';
import banner2 from '../../images/banner/banner-livros.png';

const Inicial: React.FC = () => {
    return (
        <div className="container-inicial">
            <Carousel autoplay dotPosition={'left'}> 
                <img src={banner1} alt={'1'}/>
                <img src={banner2} alt={'2'}/>
            </Carousel>
        </div>
    );
}

export default Inicial;