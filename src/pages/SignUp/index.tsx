import React from 'react';
import SignUpForm from '../../components/SignUpForm';
import { Typography } from 'antd';
import './style.css';

const {Title} = Typography;

const SignUp: React.FC = () => {
    return (
        <div className="wrapper">
            <div className="form-wrapper">
                <Title>Sign Up</Title>
                <SignUpForm />
            </div>
        </div>
    )
}

export default SignUp;