import React, { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import Descricao from '../../components/Descricao';
import { Icon } from 'antd';
import objectFactory from '../../model/objectFactory'; 
import LivroResource from '../../services/resources/LivroResource';

interface Props extends RouteComponentProps<any> {
    id: number;
}

const LivroInfo: React.FC<Props> = props => {
    // const [livroContext, setLivroContext] = useState(
    //     new objectFactory().createLivro(0, '', '', 0, [''], '')
    // );

    // const livroStore = useContext(LivroStoreContext);
    const [livro, setLivro] = useState(new objectFactory().createLivro(0, '', '', 0, [''], ''));
    const [loaded, setLoaded] = useState(false);
    const { id } = props.match.params;

    useEffect(() => {
        // try {
        LivroResource().find(id)
            .subscribe({
                next: res => { setLivro(res); },
                error: err => { throw Error(err); }
            });
        // livroStore.setLivro(id);
        setLoaded(true);
        // } catch (err) {
        //     console.log(err);
        // }
    }, [id]);

    if (loaded) {
        return (
            <div className="main-livroinfo main">
                <Descricao livro={livro}/>
            </div>);
    } else {
        return <Icon type="loading" />;
    }


};

export default LivroInfo;