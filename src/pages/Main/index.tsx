import React from 'react';
import { Layout } from 'antd';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import HeaderApp from '../../components/HeaderApp';

const { Content } = Layout;

const Main: React.FC<RouteComponentProps<any>> = (props) => {


    return (
        <React.Fragment>
            <Layout className="layout">
                <HeaderApp />
                <Content style={{
                    background: '#fff',
                    padding: 24,
                    margin: 0,
                    minHeight: 280,
                }}>
                    {props.children}
                    {/* <Switch>
                        <Route path={[`${path}/search/:titulo`, `${path}/search/`]} component={Search} />
                        <Route path={[`${path}/carrinho`]} component={Carrinho} />
                        <Route path={[`${path}/livros/:id`]} component={LivroInfo} />
                        <Route path={[`${path}/cliente`]} component={ClienteAccount} />
                    </Switch> */}
                </Content>
            </Layout>
        </React.Fragment>
    );
}

export default withRouter(Main);