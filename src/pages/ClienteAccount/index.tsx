import React, { useEffect, useState } from 'react';
import AccountSideBar from '../../components/AccountSideBar';

import DrawerPedido from '../../components/DrawerPedido';
import { observer, inject } from 'mobx-react';
import { Layout, Typography } from 'antd';
import TablePedidos from '../../components/TablePedidos';
import { ClientSesssionStore } from '../../store/ClientSessionStore';
import PedidoResource from '../../services/resources/PedidoResource';

const { Content } = Layout;
const { Title } = Typography;

interface ClienteAccountProps {
    clientSession?: ClientSesssionStore;
}

const ClienteAccout: React.FC<ClienteAccountProps> = inject('clientSession')(observer((props) => {
    // const pedidoStore = useContext(PedidoStoreContext);
    // const clientSessionStore = useContext(ClientSesssionStoreContext);
    const [drawerPedido, setDrawerPedido] = useState({ id: 0, visible: false });
    const [pedidos, setPedidos] = useState([]);

    useEffect(() => {
        console.log('ClientAccount');
        PedidoResource().getPedido(props.clientSession!.cliente.id)
            .subscribe({
                next: res => {
                    setPedidos(res.content);
                },
                error: err => { throw Error(err); }
            });
        // props.pedido!.getPedidos(props.clientSession!.cliente.id);
    }, [props.clientSession]);



    // const account = useSelector((state: StoreState) => state.clientAccount);
    // const clienteReducer = useSelector((state: StoreState) => state.clientSession.cliente);
    return (
        <div className="container-client-account">
            <Layout style={{ padding: '24px 0', background: '#fff' }}>
                <AccountSideBar />
                <Content style={{ padding: '0 24px', minHeight: 280 }}>
                    <div className={'content'}>
                        <Title level={2}>Pedidos</Title>
                        <TablePedidos setDrawerPedido={setDrawerPedido} pedidos={pedidos} />
                        <DrawerPedido state={drawerPedido} setDrawerPedido={setDrawerPedido} pedidos={pedidos} />
                    </div>
                </Content>

            </Layout>
        </div>
    )
}));

export default ClienteAccout;