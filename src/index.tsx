import React from 'react';
import ReactDOM from 'react-dom';
// import App from './App';
import Routes from './routes';
import { ConfigProvider } from 'antd';
import { Store } from './store';
import { Provider } from 'mobx-react';
import pt_BR from 'antd/lib/locale-provider/pt_BR';

ReactDOM.render((
    <ConfigProvider locale={pt_BR}>
        <Provider {...Store}>
            <Routes />
        </Provider>
    </ConfigProvider>

), document.getElementById('root'));

