import React from 'react';
import {Switch, Route, withRouter} from 'react-router-dom';
import Main from './pages/Main';
import Search from './pages/Search';
import Carrinho from './pages/Carrinho';
import LivroInfo from './pages/LivroInfo';
import ClienteAccount from './pages/ClienteAccount';
import Inicial from './pages/Inicial';

const MainRoutes: React.FC = () => {
    return (<Main>
        <Switch>
            <Route exact path={`/main/`} component={Inicial} />
            <Route path={[`/main/search/:titulo`, `/main/search/`]} component={Search} />
            <Route path={[`/main/carrinho`]} component={Carrinho} />
            <Route path={[`/main/livros/:id`]} component={LivroInfo} />
            <Route path={[`/main/cliente`]} component={ClienteAccount} />
        </Switch>
    </Main>)
}

export default withRouter(MainRoutes);