import React, { useEffect, useState } from 'react';
import { Layout, Input, Col, Row, Badge, Dropdown, Icon, Menu } from 'antd';
import logoImg from '../../images/logo.png';
import shoppIcon from '../../images/shoppingicon_white.png';
import './style.css';
import { RouteComponentProps, withRouter, Link } from 'react-router-dom';
// import { useSelector, useDispatch } from 'react-redux';
// import { StoreState } from '../../store/types';
import { ClientSesssionStore } from '../../store/ClientSessionStore';
// import { ClickParam } from 'antd/lib/menu';
import { ShoppStore } from '../../store/ShoppStore';
import { observer, inject } from 'mobx-react';

const { Header } = Layout;
const { Search } = Input;

interface HeaderApp extends RouteComponentProps<any>{
    shopp?: ShoppStore;
    clientSession?: ClientSesssionStore;
}


const HeaderApp: React.FC<HeaderApp> = inject('shopp','clientSession')(observer((props) => {

    const [itens, setItens] = useState(0);
    const carrinho = props.shopp!.getCarrinho;
    // const carrinho = useSelector((state: StoreState) => state.shopp.carrinho);
    // const client = useSelector((state: StoreState) => state.clientSession.cliente);
    // const clientSessionStore = useContext(ClientSesssionStoreContext);
    // const shoppStore = useContext(ShoppStoreContext);

    const { path } = props.match;

    function renderMenu(): JSX.Element {
        if (props.clientSession!.cliente.id) {
            return (
                <Menu>
                    <Menu.Item key="1" onClick={e => props.history.push('/main/cliente')}>
                        Pedidos
                    </Menu.Item>
                    <Menu.Item key="2" onClick={e => {
                        props.clientSession!.logOut();
                        props.history.push('/');
                    }}>
                        Log Out
                    </Menu.Item>
                </Menu>
            );

        }
        return (
            <Menu>
                <Menu.Item key="1" onClick={e => props.history.push('/')}>
                    Sign In
            </Menu.Item>
                <Menu.Item key="2" onClick={e => props.history.push('/signup')}>
                    Sign Up
            </Menu.Item>
            </Menu>
        );

    }

    function valueEntered(value: string) {
        props.history.push(`${path}/search/${value}`);
    }

    // function goToClientAccoutn() {
    //     if (client.id) {
    //         props.history.push('/main/cliente');
    //     }
    //     else {
    //         props.history.push('/');
    //     }
    // }

    useEffect(() => {
        function countItems() {
            const itens: Array<any> = (carrinho.length > 0) ? carrinho : [0];
            let retorno = itens.map(item => item.quantidade);
            return retorno.reduce((total, next) => total + next);
        }
        setItens(countItems())
    }, [carrinho]);

    return (
        <Header style={{ height: '70px' }}>
            <Row gutter={32} className="itemHeader" align="top">
                <Col span={2}>
                    <div className="img" style={{ alignContent: 'top' }}>
                        <Link to={`${path}/`}>
                            <img src={logoImg} alt="ILibrary" style={{ height: '50px' }} />
                        </Link>
                    </div>

                </Col>
                <Col span={16} className="gutter-row">
                    <Search onSearch={value => valueEntered(value)} style={{ width: '100%' }} enterButton />

                </Col>
                <Col span={2}>
                    <Link to={`${path}/carrinho`}>
                        <Badge count={itens}>
                            <img src={shoppIcon} alt="Carrinho" style={{ height: '30px' }} />
                        </Badge>
                    </Link>
                </Col>
                <Col span={4}>
                    <Dropdown.Button overlay={renderMenu()} icon={<Icon type="user" />}>
                        {props.clientSession!.cliente.nome || 'User'}
                    </Dropdown.Button>
                    {/* <div onClick={e =>goToClientAccoutn()} className="clickable">
                        <Avatar icon="user" />
                    </div> */}
                </Col>
            </Row>
        </Header>
    );
}));

export default withRouter(HeaderApp);