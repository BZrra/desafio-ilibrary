import React, { useState, useEffect } from 'react';
import { Table, Button, InputNumber } from 'antd';
// import { StoreState } from '../../store/types';
// import { useSelector, useDispatch } from 'react-redux';
import ItemShopp from '../../model/ItemShopp';
import {convertToReal} from '../../services/utils';
import { Link, withRouter, RouteComponentProps } from 'react-router-dom';
// import * as ShoppActions from '../../store/actions/shopp';
import { ShoppStore } from '../../store/ShoppStore';
import { observer, inject } from 'mobx-react';

interface TableCarrinhoProps extends RouteComponentProps<any>{
    shopp ?: ShoppStore;
}

const TableCarrinho: React.FC<TableCarrinhoProps> = inject('shopp')(observer((props) => {


    // const carrinho = useSelector((state: StoreState) => state.shopp.carrinho);
    // const dispatch = useDispatch();
    // const shoppStore = useContext(ShoppStoreContext);

    const carrinho = props.shopp!.getCarrinho;

    const [data, setData] = useState([{
        key: 0,
        titulo: '',
        autor: '',
        preco: '0',
        quantidade: 0,
    }]);
    const columns = [
        {
            title: 'Titulo',
            dataIndex: 'titulo',
            key: 'titulo',
            render: (text:string, record: any)  => <Link to={`/main/livros/${record.key}`}>{record.titulo}</Link>,
        },
        {
            title: 'Autor',
            dataIndex: 'autor',
            key: 'autor',
        },
        {
            title: 'Preço',
            dataIndex: 'preco',
            key: 'preco',
        },
        {
            title: 'Quantidade',
            dataIndex: 'quantidade',
            key: 'quantidade',
            render: (text: string, record: any) =>
                <InputNumber min={1} defaultValue={record.quantidade} onChange={value => changeAmount(value, record.key)} />
        },
        {
            title: '',
            dataIndex: 'operation',
            render: (text: string, record: any) =>
                // <Popconfirm title="Tem certeza que quer remover o livro?" onConfirm={() => deleteShopp(record.key)}>
                    <Button type="danger" size={"small"} onClick={() => deleteShopp(record.key)}>
                        Remover
                    </Button>
                // </Popconfirm>
        }
    ];

    function deleteShopp(key: number) {
        const data = [...carrinho];
        const index = data.findIndex((element: ItemShopp) => element.livro.id === key);
        data.splice(index, 1);
        props.shopp!.setShopp(data);
        // dispatch(ShoppActions.toggleShopp(data));
    }

    function changeAmount(value:number = 1, key:number){
        const data = [...carrinho];
        const item = data.find((item: ItemShopp) => item.livro.id === key);
        if(item){
            item.quantidade = value;
        }
        props.shopp!.setShopp(data);
        // dispatch(ShoppActions.toggleShopp(data));
    }


    useEffect(() => {
        setData(carrinho.map((item) => {
            const { id, titulo, autor, preco } = item.livro;
            const { quantidade } = item;
            return {
                key: id,
                titulo,
                autor,
                preco: convertToReal(preco * quantidade),
                quantidade,
            }

        }));
    }, [carrinho]);

    return <Table columns={columns} dataSource={data} locale={{emptyText: 'Carrinho Vazio!'}} bordered />
}));

export default withRouter(TableCarrinho);