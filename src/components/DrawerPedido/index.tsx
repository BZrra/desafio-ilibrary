import React from 'react';
import { Drawer } from 'antd';
import ItemPedido from '../../model/ItemPedido';
import * as Util from '../../services/utils';
// import {useSelector, useDispatch} from 'react-redux';
// import {StoreState} from '../../store/types';
// import * as AccountActions from '../../store/actions/clientAccount';
import { Typography } from 'antd';
import Pedido from '../../model/Pedido';

const { Title, Text } = Typography;

// interface Props {
//     pedido: Pedido;
//     visible: boolean;
// }

interface DrawerPedidoProps {
    state: {
        id: number;
        visible: boolean;
    };
    setDrawerPedido: React.Dispatch<React.SetStateAction<any>>;
    pedidos: Pedido[];
}

const DrawerPedido: React.FC<DrawerPedidoProps> = (props) => {

    // const account = useSelector((state: StoreState) => state.clientAccount);
    // const dispatch = useDispatch();
    // const pedidoStore = useContext(PedidoStoreContext);

    function onClose() {
        props.setDrawerPedido({id: 0, visible: false });
        // props.pedido!.setDrawerVisible(false);
        // pedidoStore.setPedido({...pedidoStore, drawerVisible: false});
        // dispatch(AccountActions.setAccount({...account, drawerVisible: false}));
    }
    if (!props.pedidos.length) {
        return <></>
    }
    else {
        return (
            <Drawer
                placement={"right"}
                closable={false}
                onClose={onClose}
                visible={props.state.visible}
            >
                <Title level={3}>Pedido Nº: {props.pedidos[props.state.id].id}</Title>
                <Text>Desconto: {props.pedidos[props.state.id].desconto}%</Text> <br />
                <Text>Data: {Util.formatData(new Date(props.pedidos[props.state.id].instante))}</Text><br />
                <Text>Itens:</Text><br />
                <ul>
                    {props.pedidos[props.state.id].itens.map((item: ItemPedido) => (
                        <li key={item.livro.id}>{item.livro.titulo}</li>
                    ))}
                </ul>
                <Text>Pagamento: {props.pedidos[props.state.id].pagamento.estado}</Text><br />
            </Drawer>
        );
    }
};

export default DrawerPedido;