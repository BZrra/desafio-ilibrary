import React from 'react';
// import { LivroContextConsumer } from '../context/livro-context';
import { Row, Col, Typography, Button } from 'antd';
import { convertToReal } from '../../services/utils';
// import LivroContext from '../../context/livro-context';
// import { useSelector, useDispatch } from 'react-redux';
// import { StoreState } from '../../store/types';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import ItemShopp from '../../model/ItemShopp';
// import * as ShoppActions from '../../store/actions/shopp';
import { ShoppStore } from '../../store/ShoppStore';
import { observer, inject } from 'mobx-react';
import ObjectFactory from '../../model/objectFactory';
import Livro from '../../model/Livro';

const { Title, Text } = Typography;

interface DescricaoProps extends RouteComponentProps<any>{
    shopp?: ShoppStore;
    livro: Livro;
}

const Descricao: React.FC<DescricaoProps> = inject('shopp')(observer((props) => {
    // const carrinho = useSelector((state: StoreState) => state.shopp.carrinho);
    // const dispatch = useDispatch();
    const objectFactory = new ObjectFactory();
    // const shoppStore = useContext(ShoppStoreContext);
    // const livroStore = useContext(LivroStoreContext);
    // const context = useContext(LivroContext);

    function addShopp() {
        const data = [...props.shopp!.getCarrinho]
        const item = data.find((item: ItemShopp) => item.livro.id === props.livro.id);
        if (item)
            item.quantidade++;
        else
            data.push(objectFactory.createItemShopp(props.livro, 1))
        props.shopp!.setShopp(data);
        // dispatch(ShoppActions.toggleShopp(data));
        props.history.push('/main/carrinho')
    }


    return (
            <Row>
                <Col span={6}>
                    <img className="img-livro"
                        src={props.livro.linkCapa}
                        alt="new"
                    />
                </Col>
                <Col span={12}>
                    <Title>{props.livro.titulo}</Title>
                    <Text>Autor: {props.livro.autor}</Text>
                    <br />
                    <Text>Preço: {convertToReal(props.livro.preco)}</Text>
                    <br />
                    <Text>Gênero(s)</Text>
                    <br />
                    <ul>
                        {props.livro.generos.map(genero => (
                            <li key={genero}>{genero}</li>
                        ))}
                    </ul>
                    <Button type="primary" block onClick={event => addShopp()} style={{width: '100%'}}>
                        Adicionar ao Carrinho
                    </Button>

                </Col>
            </Row>
    );
}));

export default withRouter(Descricao);