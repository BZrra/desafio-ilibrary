import React, { useEffect } from 'react';
import { Card, Button } from 'antd';
import Livro from '../../model/Livro';
// import { useSelector, useDispatch } from 'react-redux';
import { RouteComponentProps, withRouter, Link } from 'react-router-dom';
// import { StoreState } from '../../store/types';
import ItemShopp from '../../model/ItemShopp';
import { convertToReal } from '../../services/utils';
// import * as ShoppActions from '../../store/actions/shopp';
import { observer, inject } from 'mobx-react';
import { ShoppStore } from '../../store/ShoppStore';
import ObjectFactory from '../../model/objectFactory';

interface Props extends RouteComponentProps<any> {
    livro: Livro;
    shopp?: ShoppStore;
}

const CardsLivros: React.FC<Props> = inject('shopp')(observer((props) => {
    // const shoppStore = useContext(ShoppStoreContext);
    const objectFactory = new ObjectFactory();
    // const carrinho = useSelector((state: StoreState) => state.shopp.carrinho);
    // const dispatch = useDispatch();

    useEffect(() => console.log(`context: ${props.shopp!.getCarrinho.length}`), [props.shopp]);

    function addShopp() {
        const data = [...props.shopp!.getCarrinho]
        const item = data.find((item: ItemShopp) => item.livro.id === props.livro.id);
        if (item)
            item.quantidade++;
        else
            data.push(objectFactory.createItemShopp(props.livro, 1));
        props.shopp!.setShopp(data);
        // dispatch(ShoppActions.toggleShopp(data));
        props.history.push('/main/carrinho');
    }

    return (
        <Card
            bordered={true}
            style={{ width: 250, height: 581 }}
            cover={
                <img alt="example" src={props.livro.linkCapa} style={{ height: 363.8 }} />
            }
        >
            <Link to={`/main/livros/${props.livro.id}`}>
                {props.livro.titulo}
            </Link>
            <p>{props.livro.autor}</p>
            <p>{convertToReal(props.livro.preco)}</p>
            <Button type="primary" block onClick={event => addShopp()}>
                Adicionar ao Carrinho
            </Button>
        </Card>
    );
}));

export default withRouter(CardsLivros);