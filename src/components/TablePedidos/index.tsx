import React, { useState, useEffect } from 'react';
import { Table, Button, Icon, Empty } from 'antd';
import { withRouter, RouteComponentProps } from 'react-router-dom';
// import PedidoResource from '../../services/resources/PedidoResource';
import Pedido from '../../model/Pedido';
// import Cliente from '../../model/Cliente';
// import Pagamento from '../../model/Pagamento';
import { formatData } from '../../services/utils';
// import { useDispatch } from 'react-redux';
// import * as AccountActions from '../../store/actions/clientAccount';


interface TablePedidosProps extends RouteComponentProps {
    setDrawerPedido: React.Dispatch<React.SetStateAction<any>>;
    pedidos: Pedido[];
};


const TablePedidos: React.FC<TablePedidosProps> = (props) => {

    // const dispatch = useDispatch();
    // const pedidoStore = useContext(PedidoStoreContext);
    const [data, setData] = useState([{
        key: 0,
        id: 0,
        data: '',
        desconto: 0,
        detalhes: {}
    }]);
    const [loading, setLoading] = useState(true);
    // const pedidos = props.pedido!.pedidos;

    const columns = [
        {
            title: 'Número do pedido',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Data',
            dataIndex: 'data',
            key: 'data',
        },
        {
            title: 'Desconto(%)',
            dataIndex: 'desconto',
            key: 'desconto',
        },
        {
            title: '',
            dataIndex: 'detalhes',
            render: (pedido: Pedido, record: any) =>
                <Button type="primary" size={"small"} onClick={e => { showDrawer(record) }}>
                    + Detalhes
                </Button>
        }
    ];

    function showDrawer(record: any) {
        props.setDrawerPedido({id: record.key,visible:true});
        // props.pedido!.setDrawerPedido(record.key)
        // props.pedido!.setDrawerVisible(true);
        // dispatch(AccountActions.setAccount({
        //     pedido,
        //     drawerVisible: true
        // }));
    }


    useEffect(() => {

        // await pedidoStore.setPedidos(params.idCliente);
        // const response = await PedidoResource().getPedido(params.idCliente);
        console.log(props.pedidos.length)
        setData(props.pedidos.map((pedido: Pedido) => {
            return ({
                key: pedido.id,
                id: pedido.id,
                data: formatData(new Date(pedido.instante)),
                desconto: pedido.desconto,
                detalhes: pedido
            });
        }));
        setLoading(false);
    }, [props.pedidos]);
    if(loading){
        return <Icon type="loading" />;
    }else{
        if(props.pedidos.length){
            return <Table columns={columns} dataSource={data} locale={{ emptyText: 'Nenhum pedido realizado!' }} bordered />
        }
        else{
            return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="Nenhum pedido realizado!" />
        }
    }
};

export default withRouter(TablePedidos);