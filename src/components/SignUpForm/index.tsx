import React, { useState } from 'react';
import { Form, Icon, Input, Button, message } from 'antd';
import { FormComponentProps } from 'antd/es/form';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import ClienteResource from '../../services/resources/ClienteResource';
import { validateForm } from '../../services/utils';
// import { setSessionCookie } from '../../services/sessions';
import './style.css';
// import ClienteNewDTO from '../../model/dto/ClienteNewDTO';
import ObjectFactory from '../../model/objectFactory';

interface Props extends FormComponentProps, RouteComponentProps { };

const SignUpForm: React.FC<Props> = (props) => {
    const objectFactory = new ObjectFactory();
    const [cliente, setCliente] = useState(objectFactory.createClienteNewDTO('', '', '', ''));
    const [loading, setLoading] = useState(false);

    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 16 },
        },
    };


    function handleSubmit(e: any) {
        // console.log(cliente);
        e.preventDefault();
        if (validateForm(props)) {
            setLoading(true);
            ClienteResource().postClient(cliente)
                .subscribe({
                    next: res => {
                        console.log('Incluido');
                    },
                    error: e => {
                        throw Error(e);
                    },
                    complete: () => { message.success('Cliente incluido com sucesso!'); }
                });
            setLoading(false);
            props.history.push('/');
        }

        // e.preventDefault();
        // setLoading(true);

        // await setTimeout(() => null, 100);
        // setSessionCookie({ email });
        // props.history.push("/");
        // setLoading(false);
    };

    const { getFieldDecorator } = props.form;

    if (loading) {
        return <Icon type="loading" />;
    }
    return (
        <Form  {...formItemLayout} onSubmit={handleSubmit} style={{ justifyContent: 'center' }}>
            <Form.Item label='Nome'>
                {getFieldDecorator('nome', {
                    rules: [{ required: true, message: 'Informe seu nome de usuario!' },
                    ],
                })(
                    <Input
                        onChange={e => setCliente({ ...cliente, nome: e.target.value })}
                    />,
                )}
            </Form.Item>
            <Form.Item label="E-mail">
                {getFieldDecorator('email', {
                    rules: [
                        { required: true, message: 'Informe seu e-mail!' },
                        { type: 'email', message: 'E-mail invalido!' }],
                })(
                    <Input
                        onChange={e => setCliente({ ...cliente, email: e.target.value })}
                    />,
                )}
            </Form.Item>
            <Form.Item label="CPF">
                {getFieldDecorator('cpf', {
                    rules: [
                        { required: true, message: 'Informe seu CPF!' }],
                })(
                    <Input
                        onChange={e => setCliente({ ...cliente, cpf: e.target.value })}
                    />,
                )}
            </Form.Item>
            <Form.Item label="Senha">
                {getFieldDecorator('senha', {
                    rules: [
                        { required: true, message: 'Informe sua senha!' },
                    ]
                })(
                    <Input.Password
                        onChange={e => setCliente({ ...cliente, senha: e.target.value })}
                    />,
                )}
            </Form.Item>
            <Form.Item>
                <Button type="primary" htmlType="submit" className="signup-form-button">
                    Criar Conta
            </Button>
            </Form.Item>
        </Form>
    )
}

const WrappedSignUpForm = Form.create({ name: 'normal_sign_up_form' })(withRouter(SignUpForm));

export default WrappedSignUpForm;