import React from 'react';
import { Layout, Typography } from 'antd';
// import { useSelector } from 'react-redux';
// import { StoreState } from '../../store/types';
import './style.css';
import perfilImg from '../../images/perfil/capaprofile.jpg';
import { ClientSesssionStore } from '../../store/ClientSessionStore';
import { observer,inject } from 'mobx-react';

const { Title, Paragraph } = Typography;
const { Sider } = Layout;

interface AccountSideBarProps{
    clientSession?: ClientSesssionStore;
}

const AccountSideBar: React.FC<AccountSideBarProps> = inject('clientSession')(observer((props) => {

    // const clienteReducer = useSelector((state: StoreState) => state.clientSession.cliente);
    // const clientSessionStore = useContext(ClientSesssionStoreContext);
    return (
            <Sider width={300} style={{ background: '#fff', alignContent: 'center' }}>
                <div className="sidebar-content">
                <img className="img-perfil"
                        src={perfilImg}
                        alt="new"
                    />
                    <Title level={3}>{props.clientSession!.cliente.nome}</Title>
                    <Paragraph copyable>{props.clientSession!.cliente.email}</Paragraph>
                </div>

                {/* <Menu
                    mode="inline"
                    defaultSelectedKeys={['1']}
                    defaultOpenKeys={['sub1']}
                    style={{ height: '100%' }}
                >
                    <SubMenu
                        key="sub1"
                        title={
                            <span>
                                <Icon type="user" />
                                subnav 1
                </span>
                        }
                    >
                        <Menu.Item key="1">option1</Menu.Item>
                        <Menu.Item key="2">option2</Menu.Item>
                        <Menu.Item key="3">option3</Menu.Item>
                        <Menu.Item key="4">option4</Menu.Item>
                    </SubMenu>
                </Menu> */}

            </Sider>

    )
}));

export default AccountSideBar;