import React, { useState, useEffect } from 'react';
import { Card, Typography, Button, Table, message } from 'antd';
// import { useSelector } from 'react-redux';
// import { StoreState } from '../../store/types';
// import ItemPedidoDTO from '../../model/dto/ItemPedidoDTO';
// import PedidoNewDTO from '../../model/dto/PedidoNewDTO';
import { calcdiscount, convertToReal } from '../../services/utils';
import PedidoResource from '../../services/resources/PedidoResource';
// import * as ShoppActions from '../../store/actions/shopp';
import { observer, inject } from 'mobx-react';
import { ShoppStore } from '../../store/ShoppStore';
import { ClientSesssionStore } from '../../store/ClientSessionStore';
// import { PedidoStoreContext } from '../../store/PedidoStore';
import ObjectFactory from '../../model/objectFactory';
const { Title } = Typography;

const columns = [
    {
        title: 'Conteudo',
        dataIndex: 'conteudo',
        key: 'conteudo',
    },
    {
        title: 'Valor',
        dataIndex: 'valor',
        key: 'valor',
    },
]

interface CardSumarioProps {
    shopp?: ShoppStore;
    clientSession?: ClientSesssionStore
}

const CardSumario: React.FC<CardSumarioProps> = inject('shopp', 'clientSession')(observer((props) => {

    // const carrinho = useSelector((state: StoreState) => state.shopp.carrinho);
    // const cliente = useSelector((state: StoreState) => state.clientSession.cliente);
    // const dispatch = useDispatch();
    const objectFactory = new ObjectFactory();
    // const clientSessionStore = useContext(ClientSesssionStoreContext);
    // const pedidoStore = useContext(PedidoStoreContext); 
    // const shoppStore = useContext(ShoppStoreContext);

    const [data, setData] = useState([{
        key: 0,
        conteudo: '',
        valor: '0',
    }]);

    const [pedido, setPedido] = useState(objectFactory.createPedidoNewDTO(0, 0, []));

    useEffect(() => {
        const totalProd = props.shopp!.getCarrinho.reduce((total, next) => total + (next.livro.preco * next.quantidade), 0);
        const discount = calcdiscount(props.shopp!.getCarrinho.length);
        const totalValor = discount > 0 ? totalProd - (totalProd * (calcdiscount(totalProd) / 100)) : totalProd;
        const itens = props.shopp!.getCarrinho.map((item) => {
            return objectFactory.createItemPedidoDTO(item.livro.id, item.quantidade, (item.livro.preco * item.quantidade));
        });
        setPedido(objectFactory.createPedidoNewDTO(props.clientSession!.cliente.id, discount, itens));
        setData([{
            key: 1,
            conteudo: 'Total Produtos',
            valor: convertToReal(totalProd),
        },
        {
            key: 2,
            conteudo: 'Desconto',
            valor: `${discount}%`,
        },
        {
            key: 3,
            conteudo: 'Valor Total',
            valor: convertToReal(totalValor),
        }
        ]);
    }, [props.shopp, props.clientSession, objectFactory]);

    function checkOut() {
        PedidoResource().postPedido(pedido)
            .subscribe({
                next: res => {
                    console.log(res);
                    props.shopp!.checkOut();
                    // dispatch(ShoppActions.checkOut());
                    // await pedidoStore.getPedidos(clientSessionStore.cliente.id);
                },
                error: err => {
                    console.error(err);
                    message.error('Erro ao finalizar pedido!');
                },
                complete: () => { message.success('Pedido finalizado com sucesso!'); }


            });

        // const response = PedidoResource().postPedido(pedido);
        // if (response) {
        //     props.shopp!.checkOut();
        //     // dispatch(ShoppActions.checkOut());
        //     if (response)
        //         message.success('Pedido finalizado com sucesso!');
        //     // await pedidoStore.getPedidos(clientSessionStore.cliente.id);
        // } else {
        //     message.error('Erro ao finalizar pedido!');
        // }

    }

    return (
        <div className="container-sumario" style={{ width: '350px' }}>
            <Card title={<Title level={3}> Súmario </Title>} bordered={true} style={{ width: '350px' }}>
                <Table columns={columns} dataSource={data} showHeader={false} pagination={false} locale={{ emptyText: 'Carrinho Vazio!' }} bordered />
            </Card>
            <Button type="primary" block onClick={e => checkOut()} >
                Finalizar Compra
            </Button>
        </div>
    )
}));

export default CardSumario;