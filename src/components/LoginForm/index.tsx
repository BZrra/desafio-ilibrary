import React, { useState } from 'react';
import { Form, Input, Icon, Button, Checkbox, message } from 'antd';
import { FormComponentProps } from 'antd/es/form';
// import { setSessionCookie } from '../../services/sessions';
import { RouteComponentProps, Link, withRouter } from 'react-router-dom';
// import { useDispatch } from 'react-redux';
import { observer, inject } from 'mobx-react';
import { ClientSesssionStore } from '../../store/ClientSessionStore';
// import * as ClientSessionActions from '../../store/actions/clientSession';
import { validateForm } from '../../services/utils';

import './style.css';
// import UserSS from '../../model/UserSS';
import ObjectFactory from '../../model/objectFactory';
// import { PedidoStoreContext } from '../../store/PedidoStore';
interface Props extends FormComponentProps, RouteComponentProps<any> {
    clientSession?: ClientSesssionStore;
};

const LoginForm: React.FC<Props> = inject('clientSession')(observer((props) => {

    const [userSS, setUserSS] = useState(new ObjectFactory().createUserSS('', ''));
    const [loading, setLoading] = useState(false);
    // const clientSessionStore = useContext(ClientSesssionStoreContext);
    // const pedidoStore = useContext(PedidoStoreContext);
    // const dispatch = useDispatch();

    function handleSubmit(e: any) {
        e.preventDefault();
        setLoading(true);
        if (validateForm(props)) {
            // try {
            //     // const client = await ClienteResource().login(userSS);
            //     await props.clientSession!.setClient(userSS);
            //     // await pedidoStore.setPedidos(clientSessionStore.cliente.id);
            //     props.history.push('/main');

            // } catch (e) {
            //     message.error('Login Invalido!');
            // }
            props.clientSession!.setClient(userSS)
                .subscribe({
                    next: res => { console.log('Autenticado!') },
                    complete: () => {
                        setLoading(false);
                        props.history.push('/main');
                    },
                    error: err => {
                        message.error(err);
                        setLoading(false);
                    },
                });
        }
        // dispatch(ClientSessionActions.setClient(client));
    };

    const { getFieldDecorator } = props.form;

    if (loading) {
        return <Icon type="loading" />;
    }
    return (
        <Form onSubmit={handleSubmit}>
            <Form.Item>
                {getFieldDecorator('usuario', {
                    rules: [{ required: true, message: 'Informe seu nome de usuario!' }],
                })(
                    <Input
                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder="Usuario"
                        onChange={e => setUserSS({ ...userSS, email: e.target.value })}
                    />,
                )}
            </Form.Item>
            <Form.Item>
                {getFieldDecorator('password', {
                    rules: [{ required: true, message: 'Informe sua senha!' }],
                })(
                    <Input
                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        type="password"
                        placeholder="Senha"
                        onChange={e => setUserSS({ ...userSS, senha: e.target.value })}
                    />,
                )}
            </Form.Item>
            <Form.Item>
                {getFieldDecorator('remember', {
                    valuePropName: 'checked',
                    initialValue: true,
                })(<Checkbox>Lembe-se de mim</Checkbox>)}
                {/* <a className="login-form-forgot" href="">
                Forgot password
            </a> */}
                <Button type="primary" htmlType="submit" className="login-form-button">
                    Log in
            </Button>
                Ou <Link to={'/signup'}>Criar uma conta!</Link>
            </Form.Item>
        </Form>
    )
}));

const WrappedLoginForm = Form.create({ name: 'normal_login' })(withRouter(LoginForm));

export default WrappedLoginForm;