import React from 'react';
import { Route, Redirect, RouteProps } from 'react-router';
import {message} from 'antd';

interface Props extends RouteProps {
    authenticated: boolean;
    redirectPath:string;
    accessDenied: string;
}

export default class AuthenticatedRoute extends Route<Props> {
    render() {
        if (this.props.authenticated) {
            return super.render();
        } else {
            message.warn(this.props.accessDenied);
            return <Redirect to={this.props.redirectPath} />;
        }
    }
};