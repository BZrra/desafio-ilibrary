import Livro from './Livro';

export default class ItemPedido{
    quantidade: number;
    total: number;
    livro: Livro;

    constructor(quantidade: number, total: number, livro: Livro){
        this.quantidade = quantidade;
        this.total = total;
        this.livro = livro;
    }
}