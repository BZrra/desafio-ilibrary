const UserException = (message: string) => (
    {
        message,
        name: "UserException",
    }
)

export default UserException;