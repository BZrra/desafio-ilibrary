import Livro from '../model/Livro';
import ItemShopp from './ItemShopp';
import PedidoNewDTO from './dto/PedidoNewDTO';
import ItemPedidoDTO from './dto/ItemPedidoDTO';
import UserSS from './UserSS';
import ClienteNewDTO from './dto/ClienteNewDTO';
import Cliente from './Cliente';

class ObjectFactory {
    createLivro = (id: number, titulo: string, autor: string, preco: number, generos: [string], linkCapa: string): Livro => {
        return new Livro(id, titulo, autor, preco, generos, linkCapa);
    }
    createItemShopp = (livro: Livro, quantidade: number): ItemShopp => {
        return new ItemShopp(livro, quantidade);
    }
    createPedidoNewDTO = (idCliente: number, desconto: number, itens: ItemPedidoDTO[]) => {
        return new PedidoNewDTO(idCliente, desconto, itens);
    }
    createItemPedidoDTO = (idLivro: number, quantidade: number, total: number) => {
        return new ItemPedidoDTO(idLivro, quantidade, total);
    }
    createUserSS = (email: string, senha: string) => {
        return new UserSS(email, senha);
    }

    createClienteNewDTO = (nome: string, email: string, cpf: string, senha: string) => {
        return new ClienteNewDTO(nome, email, cpf, senha);
    }
    createCliente = (id: number, nome: string, cpf: string, email: string) => {
        return new Cliente(id, nome, cpf, email);
    }
}

export default ObjectFactory;