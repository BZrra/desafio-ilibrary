export default class Pagamento{
    id: number;
    estado: string;
    constructor(id: number, estado: string){
        this.id = id;
        this.estado = estado;
    }
}