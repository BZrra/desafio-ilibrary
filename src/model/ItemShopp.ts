import Livro from './Livro';
export default class ItemShopp{
    livro: Livro;
    quantidade: number;
    constructor(livro:Livro, quantidade: number){
        this.livro = livro;
        this.quantidade = quantidade;
    }
}