import Cliente from "./Cliente";
import ItemPedido from './ItemPedido';
import Pagamento from "./Pagamento";
export default class Pedido{
    id: number;
    desconto: number;
    instante: Date;
    cliente: Cliente;
    itens: ItemPedido[];
    pagamento: Pagamento;

    constructor(id: number, 
        desconto: number, 
        instante: Date, 
        cliente: Cliente, 
        itens: ItemPedido[],
        pagamento: Pagamento){
            this.id = id;
            this.desconto = desconto;
            this.instante = instante;
            this.cliente = cliente;
            this.itens = itens;
            this.pagamento = pagamento;
        }
}