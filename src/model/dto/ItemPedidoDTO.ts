export default class ItemPedidoDTO{
    idLivro:number;
    quantidade: number;
    total: number;
    constructor(idLivro: number, quantidade: number, total: number){
        this.idLivro = idLivro;
        this.quantidade = quantidade;
        this.total = total;
    }
}