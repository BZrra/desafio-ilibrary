import ItemPedidoDTO from './ItemPedidoDTO';

export default class PedidoNewDTO{
    idCliente: number;
    desconto: number;
    itens : ItemPedidoDTO[];

    constructor (idCliente: number, desconto: number, itens: ItemPedidoDTO[]){
        this.idCliente = idCliente;
        this.desconto = desconto;
        this.itens = itens;
    }
}