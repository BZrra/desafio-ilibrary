export default class Livro{
    id:number;
    titulo:string;
    autor:string;
    preco:number;
    generos:[string];
    linkCapa:string;
    constructor(id:number, titulo:string, autor:string, preco:number,generos:[string], linkCapa: string){
        this.id = id;
        this.titulo = titulo;
        this.autor = autor;
        this.preco = preco;
        this.generos = generos;
        this.linkCapa = linkCapa;
    }
}