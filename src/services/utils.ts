import {message} from 'antd';
import { useRef } from 'react';

export function convertToReal(value: number): string {
    return `R$ ${parseFloat(value.toFixed(2))}`
}

export function calcdiscount(quantLivros: number): number {
    if (quantLivros >= 2) {
        if (quantLivros >= 3) {
            if (quantLivros >= 4) {
                if (quantLivros >= 5) {
                    return 25;
                }
                return 20;
            }
            return 10;
        }
        return 5;
    } else {
        return 0;
    }
}

export function formatData(data: Date) {
    let day = data.getUTCDate().toString();
    day = day.length === 1 ? `0${day}` : day; 
    let month = data.getUTCMonth().toString();
    month = month.length === 1 ? `0${month}` : month;
    let year = data.getUTCFullYear().toString();
    return `${day}/${month}/${year}`;
}

export function validateForm(props: any): boolean {
    let r = true;
    props.form.validateFields((err: any, values: any) => {
        if (err) {
            message.error('Campos invalidos!');
            r = false;
        }
    });
    return r;
}

export const useCountRenders = () => {
    const renders = useRef(0);
    console.log(`renders: ${renders.current++}`)
}