import api from '../api';
import { Observable, Observer } from 'rxjs';

// const GeneroResource = () =>({
//             async getGenero() {
//                 const response = await api.get(`/generos`);
//                 return response.data;
//             }
//         }
// );

const GeneroResource = () => ({
    getGenero(): Observable<any> {
        return Observable.create(
            (obs: Observer<any>) => {
                api.get(`/generos`)
                    .then(res => res.data)
                    .then(res => {
                        obs.next(res);
                        obs.complete();
                    })
                    .catch(e => obs.error(e));
            }
        );
    }
}
);





export default GeneroResource;