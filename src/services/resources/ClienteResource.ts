import api from "../api";
import ClienteNewDTO from "../../model/dto/ClienteNewDTO";
import UserSS from '../../model/UserSS';
import { Observable, Observer } from "rxjs";

// const ClienteResource = () => ({
//     async geClient(id: number) {
//         const response = await api.get(`/clientes/${id}`);
//         return response.data;
//     },
//     async postClient(clienteNewDTO: ClienteNewDTO) {
//         try {
//             await api.post('clientes', clienteNewDTO);
//             return true;
//         } catch (e) {
//             return false;
//         }
//     },
//     async login(userSS: UserSS) {
//         const response = await api.get(`/clientes/login?email=${userSS.email}&senha=${userSS.senha}`);
//         if (!response.data)
//             throw UserException('Login invalido!');
//         return response.data;
//     }
// });

const ClienteResource = () => ({
    geClient(id: number): Observable<any> {
        return Observable.create(
            (obs: Observer<any>) => {
                api.get(`/clientes/${id}`)
                    .then(res => res.data)
                    .then(res => {
                        obs.next(res);
                        obs.complete();
                    }).catch(e => obs.error(e));
            }
        );
    },
    postClient(clienteNewDTO: ClienteNewDTO): Observable<any> {
        return Observable.create(
            (obs: Observer<any>) => {
                api.post('clientes', clienteNewDTO)
                    .then(res => {
                        obs.next(res);
                        obs.complete();
                    })
                    .catch(e => obs.error(e));
            }
        );
    },
    login(userSS: UserSS):Promise<any> {
        return api.get(`/clientes/login?email=${userSS.email}&senha=${userSS.senha}`);
    }
});

export default ClienteResource;
