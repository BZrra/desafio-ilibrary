import api from '../api';
import PedidoNewDTO from '../../model/dto/PedidoNewDTO';
import { Observable, Observer } from 'rxjs';


// const PedidoResource = () => ({
//     async getPedido(idCliente: number = 0, page: number = 0) {
//         const response = await api.get(`/pedidos?idCliente=${idCliente}&page=${page}`);
//         return response.data;
//     },
//     async postPedido(pedidoNewDTO: PedidoNewDTO) {
//         try {
//             await api.post('pedidos', pedidoNewDTO);
//             return true;
//         } catch (e) {
//             return false;
//         }
//     }
// });

const PedidoResource = () => ({
    getPedido(idCliente: number = 0, page: number = 0): Observable<any> {
        return Observable.create(
            (obs: Observer<any>) => {
                api.get(`/pedidos?idCliente=${idCliente}&page=${page}`)
                    .then(res => res.data)
                    .then(res => obs.next(res))
                    .catch(err => obs.error(err));
            }
        );
    },
    postPedido(pedidoNewDTO: PedidoNewDTO): Observable<any>{
        return Observable.create(
            (obs: Observer<any>) => {
                api.post('pedidos', pedidoNewDTO)
                    .then(res => { 
                        obs.next(res);
                        obs.complete();
                     })
                    .catch( e => obs.error(e));
            }
        );
    }
});

export default PedidoResource;

// export default class PedidoResource {
//     async getPedido(idCliente: number = 0, page: number = 0) {
//         const response = await api.get(`/pedidos?idCliente=${idCliente}&page=${page}`);
//         return response.data;
//     }
//     async postPedido(pedidoNewDTO: PedidoNewDTO) {
//         try {
//             await api.post('pedidos', pedidoNewDTO);
//             return true;
//         } catch (e) {
//             return false;
//         }
//     }
// }