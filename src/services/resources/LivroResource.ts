import api from '../api';
import { Observable, Observer } from 'rxjs';

// const LivroResource = () => ({
//     async findPage(titulo: string = '', idGenero: string = '', page: number = 0) {
//         const response = await api.get(`/livros?titulo=${titulo}&idGenero=${idGenero}&page=${page}`);
//         return response.data;
//     },
//     async find(id: number) {
//         const response = await api.get(`/livros/${id}`);
//         return response.data;
//     }
// });

const LivroResource = () => ({
    findPage(titulo: string = '', idGenero: string = '', page: number = 0): Observable<any> {
        return Observable.create((obs: Observer<any>) => {
            api.get(`/livros?titulo=${titulo}&idGenero=${idGenero}&page=${page}`)
                .then(res => res.data)
                .then(res => {
                    obs.next(res);
                    obs.complete();
                })
                .catch(err => obs.error(err));
        });
    },
    find(id: number): Observable<any> {
        return Observable.create((obs: Observer<any>) => {
            api.get(`/livros/${id}`)
                .then(res => res.data)
                .then(res => {
                    obs.next(res);
                    obs.complete();
                })
                .catch(err => obs.error(err));
        });
    }
});
export default LivroResource;

// export default class LivroResource{
//     async findPage(titulo: string = '', idGenero:string = '', page:number = 0){
//         const response = await api.get(`/livros?titulo=${titulo}&idGenero=${idGenero}&page=${page}`);
//         return response.data;
//     }

//     async find(id: number){
//         const response = await api.get(`/livros/${id}`);
//         return response.data;
//     }


// }