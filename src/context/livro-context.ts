import React from 'react';
import Livro from '../model/Livro';
import objectFactory from '../model/objectFactory';

const ctxt = React.createContext<Livro>(new objectFactory().createLivro(0,'','',0,[''], ''));

export const LivroContextProvider = ctxt.Provider;

export const LivroContextConsumer = ctxt.Consumer;

export default ctxt;