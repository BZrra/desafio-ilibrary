import ItemShopp from '../model/ItemShopp';
import Cliente from '../model/Cliente';
import Pedido from '../model/Pedido';

export interface StoreState {
    shopp: {
        carrinho: ItemShopp[];
    },
    clientSession: {
        cliente: Cliente;
    },
    clientAccount: {
        pedido: Pedido;
        drawerVisible: boolean;
    }
}

export interface ShoppState {
    carrinho: ItemShopp[],
}

export interface ClientSessionState {
    cliente: Cliente,
}

export interface ClientAccountState {
    pedido: Pedido;
    drawerVisible: boolean;
}



export interface Action {
    type: string,
    data: any;
}

// import Livro from './Livro';
// export default class ItemShopp{
//     livro: Livro;
//     quantidade: number;
//     constructor(livro:Livro, quantidade: number){
//         this.livro = livro;
//         this.quantidade = quantidade;
//     }
// }