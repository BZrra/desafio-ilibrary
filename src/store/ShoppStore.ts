import { observable, action, computed } from 'mobx';
import ItemShopp from '../model/ItemShopp';

export class ShoppStore{

    @observable private carrinho: ItemShopp[];

    constructor(){
        this.carrinho = JSON.parse(localStorage.getItem('Shopp') || '[]');
    }

    @action 
    setShopp(data: any[]){
        localStorage.setItem('Shopp', JSON.stringify(data));
        this.carrinho = data;
    }
    @action
    checkOut(){
        localStorage.removeItem('Shopp');
        this.carrinho = [];
    }

    @computed get getCarrinho(): ItemShopp[]{
        return this.carrinho;
    }
}

// export const ShoppStoreContext = createContext(new ShoppStore());