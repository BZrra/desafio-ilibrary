import ItemShopp from '../../model/ItemShopp';

export function toggleShopp(data: ItemShopp[]) {
    return {type: 'SET_SHOPP', data };
}

export function checkOut(){
    return { type: 'CHECK_OUT', data: [] };
}