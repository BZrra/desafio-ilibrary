import { ClientAccountState } from '../types';

export function setAccount(data: ClientAccountState) {
    return {type: 'SET_ACCOUNT', data };
}
