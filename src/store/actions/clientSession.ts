import Cliente from '../../model/Cliente';

export function setClient(data: Cliente) {
    return {type: 'SET_CLIENT', data };
}
export function LogOut(){
    return { type: 'LOG_OUT', data: {} };
}
