import Livro from '../model/Livro';
import objectFactory from '../model/objectFactory';
import { observable, action } from 'mobx';
import { createContext } from 'react';
import LivroResource from '../services/resources/LivroResource';

export class LivroStore {
    @observable
    livro: Livro;

    constructor() {
        this.livro = new objectFactory().createLivro(0, '', '', 0, [''], '');
    }

    @action
    setLivro(data: any) {
        // const response = await LivroResource().find(data);
        // this.livro = response;
        LivroResource().find(data)
            .subscribe({
                next: res => { this.livro = res; },
                error: err => { throw Error(err); }
            });
    }

}

export const LivroStoreContext = createContext(new LivroStore());
