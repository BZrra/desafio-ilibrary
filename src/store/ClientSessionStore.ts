import { observable, action } from 'mobx';
import Cliente from '../model/Cliente';
// import { createContext } from 'react';
import UserSS from '../model/UserSS';
import ClienteResource from '../services/resources/ClienteResource';
import { Observable, Observer } from 'rxjs';
import UserException from '../model/exception/UserException';

export class ClientSesssionStore {
    @observable
    cliente: Cliente;

    constructor() {
        this.cliente = JSON.parse(localStorage.getItem('Client') || '{}');
    }

    @action
    setClient(data: UserSS): Observable<any> {
        return Observable.create(
            (obs: Observer<any>) => {
                ClienteResource().login(data)
                    .then(res => res.data)
                    .then(res => {
                        if (res) {
                            localStorage.setItem('Client', JSON.stringify(res));
                            this.cliente = res;
                            obs.next(res);
                            obs.complete();
                        } else {
                            obs.error(UserException('Login invalido!'));
                        }
                    })
            }
        );
        // const client = await ClienteResource().login(data);
        // localStorage.setItem('Client', JSON.stringify(client));
        // this.cliente = client;
    }

    @action
    logOut() {
        localStorage.removeItem('Client');
        this.cliente = new Cliente(0, '', '', '');
    }
}

// export const ClientSesssionStoreContext = createContext(new ClientSesssionStore());