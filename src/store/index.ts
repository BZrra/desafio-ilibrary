import { ClientSesssionStore as ClientSession } from './ClientSessionStore';
import { ShoppStore as Shopp } from './ShoppStore';

export const Store = {
    clientSession: new ClientSession(),
    shopp: new Shopp()
}