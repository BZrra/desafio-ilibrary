import { observable, action } from 'mobx';
import Cliente from '../model/Cliente';
import Pedido from '../model/Pedido';
import Pagamento from '../model/Pagamento';
import PedidoResource from '../services/resources/PedidoResource';

export class PedidoStore {
    @observable
    pedidos: Pedido[];
    @observable
    drawerVisible: boolean;
    @observable
    drawerPedido: number;

    constructor() {
        this.pedidos = [new Pedido(0, 0, new Date(), new Cliente(0, '', '', ''), [], new Pagamento(0, ''))];
        this.drawerVisible = false;
        this.drawerPedido = 0;
    }

    @action
    getPedidos(data: number) {
        PedidoResource().getPedido(data)
            .subscribe({
                next: res => { this.pedidos = res.content },
                error: err => { throw Error(err) }
            });
        // const response = await PedidoResource().getPedido(data);
        // this.pedidos = response.content;
    }
    @action
    setDrawerVisible(data: boolean) {
        this.drawerVisible = data;
    }
    @action
    setDrawerPedido(data: number) {
        this.drawerPedido = data;
    }
}
// export const PedidoStoreContext = createContext(new PedidoStore());