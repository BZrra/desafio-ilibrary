import { ClientSessionState, Action } from '../types';

const INITIAL_STATE: ClientSessionState = {
    cliente: JSON.parse(localStorage.getItem('Client') || '{}')
};

export default function clientSession(state: any = INITIAL_STATE, action: Action) {
    switch (action.type) {
        case 'SET_CLIENT':
            localStorage.setItem('Client', JSON.stringify(action.data));
            return { cliente: action.data };
        case 'LOG_OUT':
            localStorage.removeItem('Client');
            return { cliente: action.data };
    }
    return state;
}