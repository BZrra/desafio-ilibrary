import { ClientAccountState, Action } from '../types';
import Pedido from '../../model/Pedido';
import Cliente from '../../model/Cliente';
import Pagamento from '../../model/Pagamento';

const INITIAL_STATE: ClientAccountState = {
    pedido: new Pedido(0,0,new Date(),new Cliente(0,'','',''),[],new Pagamento(0,'')),
    drawerVisible: false
};

export default function clientSession(state: any = INITIAL_STATE, action: Action) {
    switch (action.type) {
        case 'SET_ACCOUNT':
            return {
                    pedido: action.data.pedido,
                    drawerVisible: action.data.drawerVisible
                };
    }
    return state;
}