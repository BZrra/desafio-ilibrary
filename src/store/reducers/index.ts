import { combineReducers } from 'redux';

import shopp from './shopp';
import clientSession from './clientSession';
import clientAccount from './clientAccount';

const rootReducer = combineReducers({shopp, clientSession, clientAccount});

export default rootReducer;