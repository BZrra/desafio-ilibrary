import {ShoppState, Action} from '../types';

const INITIAL_STATE:ShoppState = {
    carrinho: JSON.parse(localStorage.getItem('Shopp') || '[]')};

export default function shopp(state:any = INITIAL_STATE, action:Action){
    switch(action.type){
        case 'SET_SHOPP':
            localStorage.setItem('Shopp', JSON.stringify(action.data));
            return {carrinho: action.data};
        case 'CHECK_OUT':
            localStorage.removeItem('Shopp');
            return {carrinho: action.data};
    }
    return state;
}