import React from 'react';
// import { Provider } from 'react-redux';
// import store from './store';
import Routes from './routes';
import './styles/geral.css';
const App: React.FC = () => {

  return (
    <div className="App">
        <Routes />
    </div>
  );
}

export default App;
